package edu.msu.anr.osgi.structuralintegrity.viewtool;

import com.dotcms.repackage.org.osgi.framework.BundleContext;
import com.dotcms.repackage.org.osgi.util.tracker.ServiceTracker;
import com.dotmarketing.osgi.GenericBundleActivator;
import com.dotmarketing.util.Logger;

import edu.msu.anr.osgi.structuralintegrity.service.StructuresSnapshotService;


/**
 * Activates this OSGi bundle.
 */
public class Activator extends GenericBundleActivator {

    /** ServiceTracker for the {@link StructuresSnapshotService}. */
    private ServiceTracker<StructuresSnapshotService, StructuresSnapshotService> structuresSnapshotServiceTracker;

    /**
     * Starts the OSGi bundle.
     * <p>
     *     Creates the service tracker for the {@link StructuresSnapshotService} and instantiates and registers the
     *     {@link StructuresSnapshotServiceTool}.
     * </p>
     * @param bundleContext The OSGi bundle context object.
     * @throws Exception If the {@link StructuresSnapshotService} service tracker cannot be instantiated or opened.
     */
    @Override
    public void start ( BundleContext bundleContext ) throws Exception {

        //Initializing services...
        initializeServices( bundleContext );

        // Create a ServiceTracker for the StructuresSnapshotService
        try {
            structuresSnapshotServiceTracker = new ServiceTracker<>(
                    bundleContext,
                    StructuresSnapshotService.class.getName(),
                    null
            );
            structuresSnapshotServiceTracker.open();
        } catch (Exception e) {
            Logger.error(this, e.getMessage());
            throw e;
        }

        //Registering the ViewTool service
        registerViewToolService( bundleContext, new StructuresSnapshotServiceToolInfo(structuresSnapshotServiceTracker) );
    }

    /**
     * Stops the OSGi bundle.
     * <p>
     *     Unregisters the {@link StructuresSnapshotServiceTool} and closes the {@link StructuresSnapshotService}
     *     service tracker.
     * </p>
     * @param bundleContext The OSGi bundle context object.
     * @throws Exception If the service viewtool cannot be unregistered or the service tracker cannot be closed.
     */
    @Override
    public void stop ( BundleContext bundleContext ) throws Exception {
        unregisterViewToolServices();
        structuresSnapshotServiceTracker.close();
    }

}