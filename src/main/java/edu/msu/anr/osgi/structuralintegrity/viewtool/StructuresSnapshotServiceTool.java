package edu.msu.anr.osgi.structuralintegrity.viewtool;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.List;

import com.dotcms.repackage.org.osgi.util.tracker.ServiceTracker;
import org.apache.velocity.tools.view.tools.ViewTool;

import edu.msu.anr.osgi.structuralintegrity.service.StructuresSnapshot;
import edu.msu.anr.osgi.structuralintegrity.service.StructuresSnapshotService;

/**
 * A viewtool which provides access to the {@link StructuresSnapshotService} read-only methods.
 */
public class StructuresSnapshotServiceTool implements ViewTool {

    /** ServiceTracker for the {@link StructuresSnapshotService}. */
    private final ServiceTracker<StructuresSnapshotService, StructuresSnapshotService> structuresSnapshotServiceTracker;

    /**
     * Constructs the StructuresSnapshotServiceTool.
     * @param structuresSnapshotServiceTracker A ServiceTracker tracking the {@link StructuresSnapshotService}.
     */
    public StructuresSnapshotServiceTool (ServiceTracker<StructuresSnapshotService, StructuresSnapshotService> structuresSnapshotServiceTracker) {
        this.structuresSnapshotServiceTracker = structuresSnapshotServiceTracker;
    }

    /**
     * Initializes the viewtool.
     * @param initData Data used to initialize the viewtool.
     */
	@Override
	public void init(Object initData) {
	}

    /**
     * Loads a saved snapshot from a snapshot file.
     * @param snapshotFile File containing a saved snapshot.
     * @return The snapshot saved in the given file.
     * @throws IOException If the snapshot could not be loaded and deserialized from the given file.
     * @throws ClassNotFoundException If the StructuresSnapshot class could not be loaded;
     */
    public StructuresSnapshot loadSnapshotFromFile(File snapshotFile) throws IOException, ClassNotFoundException {
        return getService().loadSnapshotFromFile(snapshotFile);
    }

    /**
     * Loads a saved snapshot from a snapshot file.
     * @param snapshotFilePath Path to a file containing a saved snapshot.
     * @return The snapshot saved in the given file.
     * @throws IOException if the snapshot could not be loaded and deserialized from the given file.
     * @throws ClassNotFoundException if the StructuresSnapshot class could not be loaded;
     */
    public StructuresSnapshot loadSnapshotFromFile(String snapshotFilePath) throws IOException, ClassNotFoundException {
        return getService().loadSnapshotFromFile(snapshotFilePath);
    }

    /**
     * Gets all saved snapshot files.
     * @return Array containing all files in the snapshots directory.
     */
    public File[] getSavedSnapshotFiles() {
        return getService().getSavedSnapshotFiles();
    }

    /**
     * Gets all saved snapshot files which meet the given filter criteria.
     * @param filter Filter object.
     * @return Array containing all files in the snapshots directory which meet the given filter criteria.
     */
    public File[] getSavedSnapshotFiles(FileFilter filter) {
        return getService().getSavedSnapshotFiles(filter);
    }

    /**
     * Gets all historic structures snapshots.
     * @return All saved snapshot files.
     * @throws IOException If the snapshot could not be loaded and deserialized from the given file.
     * @throws ClassNotFoundException If the StructuresSnapshot class could not be loaded;
     */
    public List<StructuresSnapshot> getSavedSnapshots() throws IOException, ClassNotFoundException {
        return getService().getSavedSnapshots();
    }

    /**
     * Gets the most recent saved structures snapshot.
     * @return The most recent saved structures snapshot.
     * @throws IOException if there is an error reading the snapshot from disk.
     * @throws ClassNotFoundException if the StructuresSnapshot class is not found.
     * @throws IndexOutOfBoundsException if there are no snapshots on disk.
     */
    public StructuresSnapshot getPreviousSnapshot() throws IOException, ClassNotFoundException, IndexOutOfBoundsException {
        return getService().getPreviousSnapshot();
    }

    /**
     * Gets the {@link StructuresSnapshotService} from the ServiceTracker.
     * @return The StructuresSnapshotService.
     */
    private StructuresSnapshotService getService() {
        return this.structuresSnapshotServiceTracker.getService();
    }

}
