package edu.msu.anr.osgi.structuralintegrity.viewtool;

import com.dotcms.repackage.org.osgi.util.tracker.ServiceTracker;
import org.apache.velocity.tools.view.context.ViewContext;
import org.apache.velocity.tools.view.servlet.ServletToolInfo;

import edu.msu.anr.osgi.structuralintegrity.service.StructuresSnapshotService;

/**
 * Provides metadata for and instantiates the {@link StructuresSnapshotServiceTool}.
 */
class StructuresSnapshotServiceToolInfo extends ServletToolInfo {

    /** ServiceTracker for the {@link StructuresSnapshotService}. */
    private final ServiceTracker<StructuresSnapshotService, StructuresSnapshotService> structuresSnapshotServiceTracker;

    /**
     * Instantiates this info class and sets the service tracker field.
     * @param structuresSnapshotServiceTracker The {@link StructuresSnapshotService} service tracker. The tracker
     *                                         should be opened before being passed to this constructor.
     */
    StructuresSnapshotServiceToolInfo(ServiceTracker<StructuresSnapshotService, StructuresSnapshotService> structuresSnapshotServiceTracker) {
        this.structuresSnapshotServiceTracker = structuresSnapshotServiceTracker;
    }

    /**
     * Gets the key to which the {@link StructuresSnapshotServiceTool} will be mapped in Velocity.
     * @return The Velocity key for the {@link StructuresSnapshotServiceTool}.
     */
    @Override
    public String getKey () {
        return "structuresSnapshotService";
    }

    /**
     * Gets the Velocity scope in which the {@link StructuresSnapshotServiceTool} will be available.
     * @return The Velocity scope in which the {@link StructuresSnapshotServiceTool} will be available.
     */
    @Override
    public String getScope () {
        return ViewContext.APPLICATION;
    }

    /**
     * Gets the name of the viewtool class.
     * @return The {@link StructuresSnapshotServiceTool} class name.
     */
    @Override
    public String getClassname () {
        return StructuresSnapshotServiceTool.class.getName();
    }

    /**
     * Gets an instance of the {@link StructuresSnapshotServiceTool}.
     * <p>
     *     Instantiates a {@link StructuresSnapshotServiceTool} with the {@link StructuresSnapshotService} service
     *     tracker, passes it the Velocity init data, and sets its Velocity scope.
     * </p>
     * @param initData Data used to initialize the viewtool.
     * @return An initialized instance of the {@link StructuresSnapshotServiceTool}.
     */
    @Override
    public Object getInstance ( Object initData ) {

        StructuresSnapshotServiceTool viewTool = new StructuresSnapshotServiceTool(this.structuresSnapshotServiceTracker);
        viewTool.init( initData );

        setScope( ViewContext.APPLICATION );

        return viewTool;
    }

}