This plugin provides a viewtool for the Structural Integrity StructuresSnapshotService.

# Installation
This plugin must be built from source and deployed to dotCMS' OSGi framework.

## Dependencies
This plugin requires that the [Structural Integrity Service plugin](https://gitlab.msu.edu/canr/edu.msu.anr.osgi.structuralintegrity.service) version 1.0.2 be available and loaded in your Apache Felix /load directory in order to be compiled or deployed.

## Building
In order to build this plugin, use the included Gradle wrapper to run the 'jar' task.

On Windows:
```
.\gradlew.bat jar
```

On Linux or MacOS:
```
./gradlew jar
```

The resulting .jar file should be located in './build/libs'.


## Deploying
In order to deploy the compiled plugin to dotCMS' OSGi framework, you may run the Gradle "load" task, upload the plugin .jar through dotCMS' Dynamic Plugins portlet, or manually place the plugin in the dotCMS instance's Apache Felix load directory.

### Dynamic Plugins Portlet
Use of dotCMS' Dynamic Plugins portlet is recommended to those without filesystem access to the dotCMS instance.

See [dotCMS' documentation on the Dynamic Plugins portlet](https://dotcms.com/docs/latest/osgi-plugins#Portlet).

### Load Task
This project's build.gradle file defines a task called "load" which builds this plugin, undeploys previous versions, and deploys the new version to the Apache Felix load directory. In order to run this task, you must set the environment variable "FELIXDIR" to the dotCMS instance's Apache Felix base directory. The load task is recommended for those with filesystem access to the dotCMS instance.

```
> FELIXDIR="$dotcms/dotcms_3.3.2/dotserver/tomcat-8.0.18/webapps/ROOT/WEB-INF/felix"
> export FELIXDIR
> ./gradlew load
```

### Filesystem
Apache Felix monitors its "load" and "undeploy" directories to install and uninstall plugin .jar files. You can move .jar files on the filesystem to load and unload them from the OSGi framework.
```
> mv "$dotcms/dotserver/tomcat-8.0.18/webapps/ROOT/WEB-INF/felix/load/edu.msu.anr.osgi.structuralintegrity.service-*.jar" "$dotcms/dotserver/tomcat-8.0.18/webapps/ROOT/WEB-INF/felix/undeploy"
> cp ./build/libs/*.jar "$dotcms/dotserver/tomcat-8.0.18/webapps/ROOT/WEB-INF/felix/load/"
```

# Usage
The Structural Integrity StructuresSnapshotServiceTool can be accessed in a Velocity template using the key
'$structuresSnapshotService'. The following [Structural Integrity Service](https://gitlab.msu.edu/canr/edu.msu.anr.osgi.structuralintegrity.service) methods are exposed:

## File[] getSavedSnapshotFiles()
Gets an array of all saved snapshot files. 

This method returns an array of [File](https://docs.oracle.com/javase/8/docs/api/java/io/File.html) objects. Each file
contains a Structural Integrity StructuresSnapshot.

```vtl
#set($snapshotFiles = $structuresSnapshotService.getSavedSnapshotFiles())
<ul>
#foreach($snapshotFiles as $file)
    <li>$file.getName()</li>
#end
</ul>
```

## StructuresSnapshot loadSnapshotFromFile(File snapshotFile)
Loads a StructuresSnapshot object from a given [File](https://docs.oracle.com/javase/8/docs/api/java/io/File.html).

StructuresSnapshot objects are saved to disk. This method loads a saved snapshot from a Java File. Typically used in
combination with getSavedSnapshotFiles().

```vtl
#set($snapshotFiles = $structuresSnapshotService.getSavedSnapshotFiles())
#foreach($snapshotFiles as $file)
    #set($snapshot = $structuresSnapshotService.loadSnapshotFromFile($file))
#end
```

## StructuresSnapshot getPreviousSnapshot()
Loads the most recent snapshot from disk. This is the same as using loadSnapshotFromFile() on the last snapshot file
from getSavedSnapshotFiles().

```vtl
#set($latestSnapshot = $structuresSnapshotService.getPreviousSnapshot())
<p>$latestSnapshot.timestamp</p>
```

## List<StructuresSnapshot> getSavedSnapshots()
Loads every snapshot from disk and returns them in a [List](http://docs.oracle.com/javase/8/docs/api/java/util/List.html).
Note that this method is extremely resource intensive as there are typically a large number of snapshot files and each
snapshot contains a large amount of data. This is generally not recommended, and iterating over the snapshot files
returned by getSavedSnapshotFiles() is preferred.

```vtl
#set($snapshots = $structuresSnapshotService.getSavedSnapshots())
#foreach($snapshot in $snapshots)
    <p>$snapshot.timestamp</p>
#end
```

